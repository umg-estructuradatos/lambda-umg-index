const aws = require('aws-sdk');
const s3 = new aws.S3({apiVersion: '2006-03-01'});
const sqs = new aws.SQS({apiVersion: '2012-11-05'});
const ddb = new aws.DynamoDB({apiVersion: "2012-08-10"});

exports.handler = async (event, context) => {
    const bucket = event.Records[0].s3.bucket.name;
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    const keySplit = key.split("/");
    const curso = keySplit[0];
    const codigo = keySplit[4];
    const fechaCarga = keySplit[1] + "" + keySplit[2] + "" + keySplit[3];
    const evento = context.awsRequestId;
    const sello = keySplit[1] + "/" + keySplit[2] + "/" + keySplit[3]
        + "#" + curso + "#/" + "/" + codigo;
    const tablaDDB = process.env.DYNAMODB_TABLE_PREFIX + "prueba";

    console.log("1.Archivo:" + key + "|Curso:" + curso
        + "|Obteniendo datos...");

    /* 2. Envío de la información de carga a la cola de mensajes SQS */
    console.log("2.Archivo:" + key + "|Curso:" + curso
        + "|Enviando informaci\u00f3n del archivo a la cola.");
    const sqsParams = {
        DelaySeconds: 10,
        MessageAttributes: {
            "codigo": {
                DataType: "String",
                StringValue: codigo
            },
            "sello": {
                DataType: "String",
                StringValue: sello
            },
            "fechaCarga": {
                DataType: "String",
                StringValue: fechaCarga
            },
            "curso": {
                DataType: "String",
                StringValue: curso
            },
			"evento": {
                DataType: "String",
                StringValue: evento
            },
            "bucketS3": {
                DataType: "String",
                StringValue: bucket
            },
            "ubicacionS3": {
                DataType: "String",
                StringValue: key
            }
        },
        "MessageBody": "Se ha realizado una carga de archivos en " + key,
        "QueueUrl": process.env.SQS_CARGAS_URL
    };
    var mensaje;
    try {
        mensaje = await sqs.sendMessage(sqsParams).promise();
        console.log("Env\u00edo de " + key + " a cola SQS con \u00e9xito. "
            + "MessageId:" + mensaje.MessageId);
    } catch (err) {
        console.log("ERROR al programar procesamiento del archivo: "
            + err);
    }

    /* 3. Creación del registro de la carga en DynamoDB */
    console.log("3.Archivo:" + key + "|Curso:" + curso
        + "|Creaci\u00f3n del registro DynamoDB.");
    const fechaCargaISO8601 = new Date().toISOString();
    const ddbParams = {
        Item: {
            "codigo": {
                S: codigo
            },
            "sello": {
                S: sello
            },
            "estado": {
                S: "C"
            },
            "eventId": {
                S: evento
            },
            "messageId": {
                S: mensaje.MessageId
            },
            "ubicacionS3": {
                S: key
            },
            "fechaCarga": {
                S: fechaCargaISO8601
            }
        },
        ReturnConsumedCapacity: "NONE",
        TableName: tablaDDB
    };
    try {
        await ddb.putItem(ddbParams).promise();
        console.log("Registro en DynamoDB " + codigo + " - "
            + sello + " creado con \u00e9xito.");
    } catch (err) {
        console.log("ERROR al crear el registro en DynamoDB: "
            + err);
    }
};
